;;; init.el --- Configuration of Emacs
;;
;; (c) 2000 - 2016 Arjen Wiersma
;;

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'org)
(require 'ob-tangle)

(setq init-dir (file-name-directory (or load-file-name (buffer-file-name))))
(org-babel-load-file (expand-file-name "loader.org" init-dir))
(org-babel-load-file (expand-file-name "gcal-config.org" init-dir))

(defun find-first-non-ascii-char ()
  "Find the first non-ascii character from point onwards."
  (interactive)
  (let (point)
    (save-excursion
      (setq point
            (catch 'non-ascii
              (while (not (eobp))
                (or (eq (char-charset (following-char))
                        'ascii)
                    (throw 'non-ascii (point)))
                (forward-char 1)))))
    (if point
        (goto-char point)
        (message "No non-ascii characters."))))
(put 'downcase-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(epg-gpg-program "/usr/local/MacGPG2/bin/gpg2")
 '(package-selected-packages
   (quote
	(org load-env-vars org-gcal org-journal minions moody ox-reveal org-bullets powerline mode-icons html-to-hiccup dockerfile-mode flycheck-gometalinter gotest go-eldoc go-snippets go-errcheck go-guru company-go flycheck-rust cargo racer emmet-mode web-mode clj-refactor cider restclient git-gutter magit-gitflow magit company clojure-snippets yasnippet rainbow-delimiters highlight-parentheses paredit-everywhere paredit s htmlize markdown-mode langtool request command-log-mode which-key use-package ivy-hydra guru-mode diminish counsel-projectile bm ace-window ace-jump-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-document-title ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif" :height 1.5 :underline nil))))
 '(org-level-1 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif" :height 1.75))))
 '(org-level-2 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif" :height 1.5))))
 '(org-level-3 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif" :height 1.25))))
 '(org-level-4 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif" :height 1.1))))
 '(org-level-5 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif"))))
 '(org-level-6 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif"))))
 '(org-level-7 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif"))))
 '(org-level-8 ((t (:inherit default :weight bold :foreground "white" :family "Sans Serif")))))
